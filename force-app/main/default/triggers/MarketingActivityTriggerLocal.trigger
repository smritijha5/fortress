trigger MarketingActivityTriggerLocal on leaseworks__Marketing_Activity__c (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
    
	if( LeaseWareUtilsLocal.isTriggerDisabled() )return;
	TriggerFactoryLocal.createHandler(leaseworks__Marketing_Activity__c.sObjectType);
}
trigger AssetChecklistTrigger on Asset_Checklist__c (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
   
	if( LeaseWareUtilsLocal.isTriggerDisabled() )return;
	TriggerFactoryLocal.createHandler(Asset_Checklist__c.sObjectType);
}
trigger AssetTermTriggerLocal on leaseworks__Aircraft_Proposal__c (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
   
	if( LeaseWareUtilsLocal.isTriggerDisabled() )return;
	TriggerFactoryLocal.createHandler(leaseworks__Aircraft_Proposal__c.sObjectType);
}
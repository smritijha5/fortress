public class AssetTermTriggerHandlerLocal implements ITriggerLocal{
    
    // Member variable to hold the Id's of Accounts 'in use'
    //private Set<Id> m_inUseIds = new Set<Id>();
    
    // Member variable to record Audit records
    //private List<Audit__c> m_audits = new List<Audit__c>();
    
    // Constructor
    private final string triggerBefore = 'AssetTermTriggerHandlerLocalBefore';
    private final string triggerAfter = 'AssetTermTriggerHandlerLocalAfter';
    public AssetTermTriggerHandlerLocal()
    {
    }
    
    /**
* bulkBefore
*
* This method is called prior to execution of a BEFORE trigger. Use this to cache
* any data required into maps prior execution of the trigger.
*/
    public void bulkBefore()
    {
        // If this a delete trigger Cache a list of Account Id's that are 'in use'
    }
    
    public void bulkAfter()
    {
    }
    
    public void beforeInsert()
    {
        
    }
    
    public void beforeUpdate()
    {
        
    }
    
    /**
* beforeDelete
*
* This method is called iteratively for each record to be deleted during a BEFORE
* trigger.
*/
    public void beforeDelete()
    {  

    }
    
    public void afterInsert()
    {
        copyDepartmentDefaultsOnDepartment();
    }
    
    public void afterUpdate()
    {
    }
    
    public void afterDelete()
    {        
    }
    
    public void afterUnDelete()
    {        
    }
    
    public void andFinally()
    {
    }
    //after insert 
    private void copyDepartmentDefaultsOnDepartment(){

        System.debug('AssetTermTriggerHandlerLocal.copyDepartmentDefaultsOnDepartment(+)');

        list<leaseworks__Aircraft_Proposal__c> newList = (list<leaseworks__Aircraft_Proposal__c >)trigger.new;
        list<Department__c> listDeptStatusToAdd = new list<Department__c>();
        list<Asset_Checklist__c> listAssetChckLisTotAdd = new list<Asset_Checklist__c>();
        set<Id> setAPId = new Set<Id>();
         
        for(leaseworks__Aircraft_Proposal__c curAP: newList){
            setAPId.add(curAP.Id);
        }

        list<leaseworks__Department_Default__c > listSourceDepts = [select  id, Name, (Select id , Name from Criteria_Defaults__r) from leaseworks__Department_Default__c];

        if(listSourceDepts.isEmpty()) {
            System.debug('Department Defaults Missing.');
            LeaseWareUtilsLocal.createExceptionLog(null,'Department Defaults Missing',null, null,'Insert Asset In Deal', false);
        }

        for(leaseworks__Aircraft_Proposal__c curAP: newList){
            for(leaseworks__Department_Default__c curDept: listSourceDepts){
                Department__c dept = new Department__c (Name = curDept.Name,
                                                        Asset_in_Deal__c = curAP.id);
                listDeptStatusToAdd.add(dept);
            }// For Dept Default
        }// For Aircrft Prpsal
               
        if(listDeptStatusToAdd.size()>0){
            try{
             //   System.debug('1. Number of script statements used so far BEFORE: ' +  Limits.getDmlStatements());
             //   System.debug('1. Number of queries used so far BEFORE: ' +  Limits.getQueries());
                insert listDeptStatusToAdd;
              //  System.debug('1. Number of script statements used so far AFTER : ' +  Limits.getDmlStatements());
              //  System.debug('1. Number of queries used so far BEFORE: ' +  Limits.getQueries());
            }catch(Exception e){
                System.debug(e);
                LeaseWareUtilsLocal.createExceptionLog(e,e.getMessage(),'Department__c', null,'Insert Department on Asset In Deal', false);
            }
        }  

        map<Id,List<Department__c>> mapObject = new map<Id,List<Department__c>>();
        List<Department__c> listDept1 = [Select Id, Name, Asset_in_Deal__c from Department__c where Asset_in_Deal__c in :setAPId];

        System.debug('listDept1 :' + listDept1.size() + '::' + listDept1);
        for(leaseworks__Aircraft_Proposal__c curAP: newList){
           
            List<Department__c> listDept2 = new List<Department__c>();  //To get Department specific to each Asset in Deal to set correct Department Id on Asset Checklist
            for (Department__c curDept: listDept1){
                    if(curDept.Asset_in_Deal__c == curAP.id){
                        listDept2.add(curDept);
                    }
            }
            System.debug('listDept2 :' + listDept2.size() + '::' + listDept2);
                mapObject.put(curAP.id,listDept2);
        }

        System.debug('mapObject :' + mapObject.size() + '::' + mapObject);

        map<String,List<Criteria_Default__c>> mapCrtiteriaToDepartmentDefault = new map<String,List<Criteria_Default__c>>();
        for(leaseworks__Aircraft_Proposal__c curAP: newList){     
           // List<Department__c> listAPDept = new List<Department__c>();
            for(leaseworks__Department_Default__c curDept: listSourceDepts){    
                mapCrtiteriaToDepartmentDefault.put(curDept.Name, curDept.Criteria_Defaults__r);    
            }
        }  

        System.debug('mapCrtiteriaToDepartmentDefault :' + mapCrtiteriaToDepartmentDefault.size() + '::' + mapCrtiteriaToDepartmentDefault);
                            
        for(leaseworks__Aircraft_Proposal__c curAP: newList){
            for(Department__c curDept: mapObject.get(curAP.id)){
                List<Criteria_Default__c> listCrit  = new List<Criteria_Default__c>();  
                if(mapCrtiteriaToDepartmentDefault.containsKey(curDept.Name)){
                    listCrit =  mapCrtiteriaToDepartmentDefault.get(curDept.Name);
                }
                for(Criteria_Default__c curCrit:listCrit){ 
                    Asset_Checklist__c asstChkPnt = new Asset_Checklist__c(Name = curCrit.Name,
                                                                           Department__c = curDept.id);
                    listAssetChckLisTotAdd.add(asstChkPnt);
                }              
            }
        }

        System.debug('AsstChcklist:' + listAssetChckLisTotAdd.size() + '::' + listAssetChckLisTotAdd);
       // System.debug('2.Number of script statements used so far BEFORE: ' +  Limits.getDmlStatements());
       /// System.debug('2.Total Number of Queries used in this apex code context: BEFORE' +  Limits.getQueries() );
        
        if(listAssetChckLisTotAdd.size()>0){
            try{
				LeaseWareUtilsLocal.setFromTrigger(AssetChecklistTriggerHandlerLocal.copyAssetChecklistToAllAssets);
                insert listAssetChckLisTotAdd;
            }catch(Exception e){
                System.debug(e);
                LeaseWareUtilsLocal.createExceptionLog(e,e.getMessage(),'Asset_Checklist__c', null,'Insert Asset Checklist on Department on Asset In Deal', false);
            }
        }  
      //  System.debug('2. Number of script statements used so far AFTER: ' +  Limits.getDmlStatements());
      //  System.debug('2.Total Number of Queries used in this apex code context: AFTER' +  Limits.getQueries() );
        LeaseWareUtilsLocal.insertExceptionLogs();
        System.debug('AssetTermTriggerHandlerLocal.copyDepartmentDefaultsOnDepartment(-)');
    }
}
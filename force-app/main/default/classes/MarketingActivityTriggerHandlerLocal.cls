public class MarketingActivityTriggerHandlerLocal implements ITriggerLocal{
    
    // Member variable to hold the Id's of Accounts 'in use'
    //private Set<Id> m_inUseIds = new Set<Id>();
    
    // Member variable to record Audit records
    //private List<Audit__c> m_audits = new List<Audit__c>();
    
    // Constructor
    private final string triggerBefore = 'MarketingActivityTriggerHandlerLocalBefore';
    private final string triggerAfter = 'MarketingActivityTriggerHandlerLocalAfter';
    public MarketingActivityTriggerHandlerLocal()
    {
    }
    
    /**
* bulkBefore
*
* This method is called prior to execution of a BEFORE trigger. Use this to cache
* any data required into maps prior execution of the trigger.
*/
    public void bulkBefore()
    {
        // If this a delete trigger Cache a list of Account Id's that are 'in use'
    }
    
    public void bulkAfter()
    {
    }
    
    public void beforeInsert()
    {
        
    }
    
    public void beforeUpdate()
    {
        
    }
    
    /**
* beforeDelete
*
* This method is called iteratively for each record to be deleted during a BEFORE
* trigger.
*/
    public void beforeDelete()
    {  }
    
    public void afterInsert()
    {}
    
    public void afterUpdate()
    {}
    
    public void afterDelete()
    {}
    
    public void afterUnDelete()
    {}
    
    public void andFinally()
    {}
}
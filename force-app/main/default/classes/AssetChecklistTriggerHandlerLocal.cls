// ------------ Asana: https://app.asana.com/0/1120337525881921/1166628459691625 -----------------------------

public class AssetChecklistTriggerHandlerLocal implements ITriggerLocal{
    
    // Constructor
    private final string triggerBefore = 'AssetChecklistTriggerHandlerLocalBefore';
    private final string triggerAfter = 'AssetChecklistTriggerHandlerLocalAfter';
    public static final string copyAssetChecklistToAllAssets = 'copyAssetChecklistToAllAssets';
    private static Set<Id> setDealIdComplt = new Set<Id>();
    private static Set<Id> setDealId  = new Set<Id>();
    private static Set<String> setDepartmentName = new Set<String>();
    private static List<Department__c> listDepartments = new List<Department__c>();
    private static List<leaseworks__Aircraft_Proposal__c> listAP = new List<leaseworks__Aircraft_Proposal__c> ();
    private static map<id,Department__c> mapDepartment = new map<id,Department__c>();
    private static map<id,leaseworks__Aircraft_Proposal__c> mapDeptIdToAP = new map<id,leaseworks__Aircraft_Proposal__c>();
    private static map<id,Department__c> mapACIdToDept = new map<id,Department__c>();
    private static map<String,List<String>> mapDeptToAssetChcklistName = new map<String,List<String>>();

    public AssetChecklistTriggerHandlerLocal()
    {

    }
    
    /**
* bulkBefore
*
* This method is called prior to execution of a BEFORE trigger. Use this to cache
* any data required into maps prior execution of the trigger.
*/
    public void bulkBefore()
    {
        System.debug('AssetChecklistTriggerHandlerLocal.bulkBefore(+)');
        // If this a delete trigger Cache a list of Account Id's that are 'in use'

        System.debug('AssetChecklistTriggerHandlerLocal.bulkBefore(+)');
    }
    
    public void bulkAfter()
    {
        System.debug('AssetChecklistTriggerHandlerLocal.bulkAfter(+)');

        Set<Id> setDepartmentId = new Set<Id>();

        //Get Deal id's
        setDealId = getDealId();

        // Get All Asset in Deal and Departments
        listAP = [Select id, Name, leaseworks__Marketing_Activity__c,(Select id, Name from Departments__r) 
                  from leaseworks__Aircraft_Proposal__c where leaseworks__Marketing_Activity__c in :setDealId];
        for(leaseworks__Aircraft_Proposal__c curAP: listAP){
            List<Department__c> listDept = curAP.Departments__r;
            for(Department__c curDept:listDept){
                mapDeptIdToAP.put(curDept.Id,curAP);
                setDepartmentId.add(curDept.Id);
            }
        }
        System.debug('mapAircraftProposal in bulkAfter:' + mapDeptIdToAP.size() +'::'+ mapDeptIdToAP);

           //Get All Departments with Asset Checklist          
           listDepartments = new List<Department__c>([Select Id,Name, (Select Id,Name, Is_Completed__c,Comments__c,Copy_Comment__c,
                Copy_To_All_Assets__c,Needs_Attention__c from Asset_Checklists__r) 
                from Department__c where id in:setDepartmentId]);

           for(Department__c curDept: listDepartments){
            List<String> assetChcklistName = new List<String>();
               for(Asset_Checklist__c curAC:curDept.Asset_Checklists__r){
                   mapACIdToDept.put(curAC.Id,curDept);
                   assetChcklistName.add(curAC.Name);
               }
               mapDeptToAssetChcklistName.put(curDept.name,assetChcklistName);
               mapDepartment.put(curDept.Id,curDept);
           }  
           System.debug('mapAssetChecklist in bulkAfter:' + mapACIdToDept.size() +'::'+ mapACIdToDept);

        // Get Deal Id's where complete is true.
        if( setDealIdComplt != null){
            setDealIdComplt = getDealIdForCompleted();
        }
        System.debug('setDealId in bulkAfter:' + setDealIdComplt.size() +'::'+ setDealIdComplt);
        
        //get all the Department names for which toggle has to be switched
        list<leaseworks__Department_Default__c > listSourceDepts = [select  id, Name from leaseworks__Department_Default__c];
        for(leaseworks__Department_Default__c curDept: listSourceDepts){
            setDepartmentName.add(curDept.Name);
        }
        System.debug('setDepartmentName in bulkAfter:' + setDepartmentName.size() +'::'+ setDepartmentName);

        System.debug('AssetChecklistTriggerHandlerLocal.bulkAfter(-)');
    }
    
    public void beforeInsert()
    {
        System.debug('AssetChecklistTriggerHandlerLocal.beforeInsert(+)');

        // Checking if a duplicate Checklist is not added on department.
        Set<Id> setDepartmentId = new Set<Id>();
        for(Asset_Checklist__c curAC:(list<Asset_Checklist__c>)trigger.new){
            setDepartmentId.add(curAC.Department__c);
        }
        Map<Id,Department__c> mapDepartments = new Map<Id,Department__c>([Select Id,Name, (Select Id,Name from Asset_Checklists__r) 
        from Department__c where id in:setDepartmentId]);

        for(Asset_Checklist__c curAC:(list<Asset_Checklist__c>)trigger.new){
            Map<Id,Asset_Checklist__c> mapACOnDept = new Map<Id,Asset_Checklist__c>(mapDepartments.get(curAc.Department__c).Asset_Checklists__r) ;
            System.debug('setACOnDept in beforeInsert:' + mapACOnDept.size() +'::'+ mapACOnDept);
            for(Asset_Checklist__c curRec:mapACOnDept.values()){
                if(curRec.Name.equalsIgnoreCase(curAC.Name)){
                    curAC.addError('Asset Checklist name ' +curAC.Name+ ' already exists on this Department');
                }
            }
        }

        System.debug('AssetChecklistTriggerHandlerLocal.bulkBefore(-)');
     
    }
    
    public void beforeUpdate()
    {
        
    }
    
    /**
* beforeDelete
*
* This method is called iteratively for each record to be deleted during a BEFORE
* trigger.
*/
    public void beforeDelete()
    {  
        System.debug('AssetChecklistTriggerHandlerLocal.beforeDelete(+)');

        list<Asset_Checklist__c> newList = (list<Asset_Checklist__c>)(trigger.old);
        for(Asset_Checklist__c curRec:newList){
            if(curRec.Is_Completed__c || curRec.Needs_Attention__c || String.isNotBlank(curRec.Comments__c)){
                curRec.addError('Cannot delete a checklist with values.');
            }
        }
        System.debug('AssetChecklistTriggerHandlerLocal.beforeDelete(-)');
    }
    
    public void afterInsert()
    {
       System.debug('AssetChecklistTriggerHandlerLocal.afterInsert(+)');
                
       if(LeaseWareUtilsLocal.isFromTrigger(triggerAfter)) return;
       LeaseWareUtilsLocal.setFromTrigger(triggerAfter);
        
       updateAssetChecklistToAllAssets();
       if(setDealIdComplt !=null && setDepartmentName !=null) updateToggleForDepartment(setDealIdComplt,setDepartmentName);
    
        System.debug('AssetChecklistTriggerHandlerLocal.afterInsert(-)');
    }
    
    public void afterUpdate()
    {
        System.debug('AssetChecklistTriggerHandlerLocal.afterUpdate(+)');

        if(LeaseWareUtilsLocal.isFromTrigger(triggerAfter)) return;
        LeaseWareUtilsLocal.setFromTrigger(triggerAfter); 

        updateCheckboxOnAllAssets();                                               
        if(setDealIdComplt !=null && setDepartmentName !=null) updateToggleForDepartment(setDealIdComplt,setDepartmentName);
        
        System.debug('AssetChecklistTriggerHandlerLocal.afterUpdate(-)');
    }
    
    public void afterDelete()
    {        
        System.debug('AssetChecklistTriggerHandlerLocal.afterDelete(+)');
        
        if(LeaseWareUtilsLocal.isFromTrigger(triggerAfter)) return;
        LeaseWareUtilsLocal.setFromTrigger(triggerAfter); 
        
        updateAssetChecklistToAllAssets();
        if(setDealIdComplt !=null && setDepartmentName !=null) updateToggleForDepartment(setDealIdComplt,setDepartmentName);
    
        System.debug('AssetChecklistTriggerHandlerLocal.afterDelete(-)');
    }
    
    public void afterUnDelete()
    {        
    }
    
    public void andFinally()
    {
    }

        // afterInsert // afterDelete
        // Copy or delete AssetChecklist on All Asset in Deals.
	private void updateAssetChecklistToAllAssets(){

            if(LeaseWareUtilsLocal.isFromTrigger(copyAssetChecklistToAllAssets))return;
			LeaseWareUtilsLocal.setFromTrigger(copyAssetChecklistToAllAssets);
            System.debug('AssetChecklistTriggerHandlerLocal.copyAssetChecklistToAllAssets(+)');

            System.debug('setDealId:' + setDealId);
            System.debug('mapDeptIdToAP:' + mapDeptIdToAP.size() + '::' + mapDeptIdToAP);
            System.debug('mapDeptToAssetChcklistName:' + mapDeptToAssetChcklistName.size() +'::'+ mapDeptToAssetChcklistName);

            Set<Id> setDepartmentId = new Set<Id>();
            Set<String> setDepartmentName = new Set<String>();
            Set<String> setAssetChecklistName = new Set<String>();
            Set<Id> setAssetChecklistId = new Set<Id>();
            Set<Asset_Checklist__c> setACToAddInDept =new Set<Asset_Checklist__c>();
            Set<Asset_Checklist__c> setACToDeleteInDept =new Set<Asset_Checklist__c>();
            Map<Id,Department__c> mapDepartmentToUpdate = new Map<Id,Department__c>();
            Map<String,List<String>> mapAssetChecklistNameToDept = new Map<String,List<String>>();
            Map<id,leaseworks__Aircraft_Proposal__c> mapDeptIdToAP1 = new Map<id,leaseworks__Aircraft_Proposal__c>();

            //Department Ids from Asset Checklist to update       

             list<Asset_Checklist__c> newList = (list<Asset_Checklist__c>)(trigger.isDelete?trigger.old:trigger.New);
            for(Asset_Checklist__c curAC : newList){
                setDepartmentId.add(curAC.Department__c);  // getting department id's for which asset checklist was added
                setAssetChecklistName.add(curAC.name.toLowerCase());
                setAssetChecklistId.add(curAC.Id); 
                setDepartmentName.add(mapDepartment.get(curAC.Department__c).Name);       
            } 
            
            System.debug('setAssetChecklistId:' + setAssetChecklistId +'::'+ setAssetChecklistName);
            System.debug('setDepartmentId:' + setDepartmentId +'::'+ setDepartmentName);

            for(String name: setDepartmentName){
                List<String> listACNames = new List<String>();
                for(Asset_Checklist__c curAC : newList){
                    if(mapDepartment.get(curAC.Department__c).Name.equalsIgnoreCase(name)) listACNames.add(curAc.name);
                    mapAssetChecklistNameToDept.put(name,listACNames); // If multiple Asset checklist is added for same department in bulk operation
                }
            }
           
            System.debug('mapAssetChecklistNameToDept:' + mapAssetChecklistNameToDept.size() +'::'+ mapAssetChecklistNameToDept);

            for(leaseworks__Aircraft_Proposal__c curAP: listAP){
                List<Department__c> listDept = curAP.Departments__r;
                for(Department__c dept:listDept){
                    if(setDepartmentName.contains(dept.name) && !(setDepartmentId.contains(dept.Id))){
                        mapDeptIdToAP1.put(dept.Id,curAP);
                        mapDepartmentToUpdate.put(dept.Id, mapDepartment.get(dept.Id));  // Department ids which should be updated with new Asset checklist
                    }
                }
            }                   

            System.debug('Dept to update :' + mapDepartmentToUpdate.keyset().size() +'::'+ mapDepartmentToUpdate);


            // To copy AssetChecklist to All Asset in Deals
            if(trigger.isInsert){
                System.debug('--------------------Inserting Asset Checklist-------------');
                for(Department__c curDept : mapDepartmentToUpdate.values()){
                    boolean isPresent = false;
                    System.debug('check dept name :'+ curDept.name + '::'+ curDept.Id );
                    for(Asset_Checklist__c curAC : curDept.Asset_Checklists__r){
                        System.debug('check name :'+ curAc.name + '::' + curAC.Id);
                        if((setAssetChecklistName.contains(curAC.name.toLowerCase()))){
                            isPresent = true;                       
                            break;                        
                        }
                        //else{
                        //       continue ;
                        //     }
                    }
                    if(!isPresent){
                        System.debug('Adding:');
                        for(String name : mapAssetChecklistNameToDept.get(curDept.name)){
                            if((setAssetChecklistName.contains(name.toLowerCase()))){   // Check if the correct asset checklist goes to correct dept
                                Asset_checklist__c newAC = new Asset_Checklist__c(Name = name,Department__c = curDept.Id );
                                setACToAddInDept.add(newAC);
                            }
                        }
                    }
                }
            }

            if(trigger.isDelete){
                System.debug('--------------------Deleting Asset Checklist-------------');
                for(Department__c curDept : mapDepartmentToUpdate.values()){
                    boolean isPresent = false;
                    System.debug('check dept name :'+ curDept.name + '::'+ curDept.Id );
                    for(Asset_Checklist__c curAC : curDept.Asset_Checklists__r){                        
                        if((setAssetChecklistName.contains(curAC.name.toLowerCase()))){
                            System.debug('check name :'+ curAc.name);
                            if(!(curAC.Is_Completed__c) && !(curAC.Needs_Attention__c) && String.isBlank(curAC.Comments__c)){
                                setACToDeleteInDept.add(curAC);                      
                                break; 
                            }                       
                        }
                    }
                }
            }
            
            System.debug('List to insert :' + setACToAddInDept.size() +'::'+ setACToAddInDept);
            System.debug('List to delete :' + setACToDeleteInDept.size() +'::'+ setACToDeleteInDept);
            if(setACToAddInDept.size()>0){
                try{
                    insert (new List<Asset_Checklist__c>(setACToAddInDept));
                }catch(Exception e){
                    //System.debug(e);
                    LeaseWareUtilsLocal.createExceptionLog(e,'Inserting new AssetChecklist to all assets failed','Asset_Checklist__c', '', 'Asset Checklist Insert', false);
                }
            }
            if(setACToDeleteInDept.size()>0){
                try{
                    delete (new List<Asset_Checklist__c>(setACToDeleteInDept));
                }catch(Exception e){
                   // System.debug(e);
                    LeaseWareUtilsLocal.createExceptionLog(e,'Deleting AssetChecklist from all assets failed','Asset_Checklist__c', '', 'Asset Checklist Delete', false);
                }
            }
            LeaseWareUtilsLocal.insertExceptionLogs();
            System.debug('AssetChecklistTriggerHandlerLocal.copyAssetChecklistToAllAssets(-)');
	}

        //afterInsert/afterUpdate
        @future
        private static void updateToggleForDepartment(Set<Id> setDealId, set<String> setDepartmentName){
            System.debug('AssetChecklistTriggerHandlerLocal.updateToggleForDepartment(+)');
           
            System.debug('setDealId :' + setDealId);
            System.debug('setDepartmentName :' + setDepartmentName.size() +'::'+  setDepartmentName);

            Set<id> setAPId = new Set<Id>();
            set<Department__c> setCompletedDepartment = new set<Department__c>();
            map<String,Integer> mapCompletedDepartment = new map<String,Integer>();
            map<String,Integer> mapDepartmentCountInAP = new map<String,Integer>();
            map<Id,List<leaseworks__Deal_Team__c>> mapDealTeam = new map<Id,List<leaseworks__Deal_Team__c>>();
            Id dealId ;

            for(String s:setDepartmentName){
                mapCompletedDepartment.put(s,0);  // initializing map to first value, to increase the counter later
                mapDepartmentCountInAP.put(s,0);
            }

            //Get all Aircraft Proposal(Asset in Deal) for the Deal
            List<leaseworks__Aircraft_Proposal__c> listAP = [Select id, Name, leaseworks__Marketing_Activity__c, Asset_Progress__c , (Select id, Name, Department_Progress__c from Departments__r) from leaseworks__Aircraft_Proposal__c where leaseworks__Marketing_Activity__c in:setDealId];

            for(leaseworks__Aircraft_Proposal__c curAP: listAP){
                dealId = curAP.leaseworks__Marketing_Activity__c;
                for(Department__c curDept: curAP.Departments__r){
                    if(curDept.Department_Progress__c == 100.00){
                        mapCompletedDepartment.put(curDept.Name,mapCompletedDepartment.get(curDept.Name)+1);
                    }
                    mapDepartmentCountInAP.put(curDept.Name,mapDepartmentCountInAP.get(curDept.Name)+1);
                }
            }

            System.debug('AP MAP :' + mapDepartmentCountInAP);

            List<leaseworks__Deal_Team__c> listDealTeamToUpdate = new List<leaseworks__Deal_Team__c>();
            Set<String> setDealTeamToUpdate = new Set<String>();

            //Get All Deal Teams for the Deal
            List<leaseworks__Deal_Team__c> listDealTeam =[Select id, Name, leaseworks__Marketing_Activity__c ,leaseworks__Complete__c from leaseworks__Deal_Team__c where leaseworks__Marketing_Activity__c =: dealId];
            
            System.debug('Deal Teams:'+ listDealTeam.size() +'::'+ listDealTeam);
            System.debug(mapCompletedDepartment);
                
            for(String s:setDepartmentName){
                    if(mapCompletedDepartment.containsKey(s)){
                        if(mapCompletedDepartment.get(s) == mapDepartmentCountInAP.get(s)){  // If no of completed department is same as no of that department in all Asset In Deals, then all are completed.
                                for(leaseworks__Deal_Team__c dealTeam: listDealTeam){                           
                                    if(s.equalsIgnoreCase(dealTeam.Name)){
                                        dealTeam.leaseworks__Complete__c = true;
                                        listDealTeamToUpdate.add(dealTeam);
                                    }
                                }
                            System.debug('Toggle can be on for :' + s);
                        }
                        else{
                            setDealTeamToUpdate.add(s);
                        }
                    }
                }

                // updating Complete checkbox to false on all Deal Teams which are not completed
                for(String s:setDealTeamToUpdate){
                    for(leaseworks__Deal_Team__c dealTeam: listDealTeam){ 
                        if(dealTeam.Name.equalsIgnoreCase(s)){
                            dealTeam.leaseworks__Complete__c = false;
                            listDealTeamToUpdate.add(dealTeam);
                        }
                    }
                }

                if(listDealTeamToUpdate.size()>0){
                    System.debug('Deal Team to Update:' + listDealTeamToUpdate.size() + '::' + listDealTeamToUpdate);
                    try{
                        update listDealTeamToUpdate;
                       // System.debug('2.Total Number of Queries used in this apex code context:  : ' +  Limits.getQueries() );
                       // System.debug('2.Total Number of DML used in this apex code context:  : ' +  Limits.getDmlStatements() );
                    }catch(Exception e){
                        System.debug(e);
                        LeaseWareUtilsLocal.createExceptionLog(e,e.getMessage(),'Deal_Team__c', null,'Updating Toggle Switch for Deal', false);
                    }
                }
                LeaseWareUtilsLocal.insertExceptionLogs();
                System.debug('AssetChecklistTriggerHandlerLocal.updateToggleForDepartment(-)');
        }

        //after update 
        // copies complete / comment / attention checkbox on all asset in deals.
        private void updateCheckboxOnAllAssets(){

            System.debug('AssetChecklistTriggerHandlerLocal.updateCheckboxOnAllAssets(+)');
                      
            System.debug('setDealId:' + setDealId);

            Set<Id> setAPId = new Set<Id>();
            set<Id> setToUpdate = new set<Id>();
            Set<Id> setDepartmentId = new Set<Id>();
            Set<String> setDepartmentName = new Set<String>();
            Set<String> setAssetChecklistName = new Set<String>();
            list<Asset_Checklist__c> listToUpdate = new list<Asset_Checklist__c>();
            Map<Id,Department__c> mapDepartmentToUpdate = new Map<Id,Department__c>();
            Map<String,Asset_CheckList__c> mapAssetChecklist = new Map<String,Asset_CheckList__c>();
            
            
            set<Asset_Checklist__c> newSetWithCheckboxOn = new set<Asset_Checklist__c>();
            
            list<Asset_Checklist__c> newList = (list<Asset_Checklist__c>)trigger.new ;
            System.debug('Size of new Asset checklist :' + newList.size());

            // getting asset checklist for which checkboxes are true
            for(Asset_Checklist__c curRec: newList){
                if(curRec.Copy_To_All_Assets__c){
                    newSetWithCheckboxOn.add(curRec);
                }
                if(curRec.Copy_Comment__c){
                    newSetWithCheckboxOn.add(curRec);
                }
                if(curRec.Copy_Attention__c){
                    newSetWithCheckboxOn.add(curRec);
                }
            }
            System.debug('Size of new Asset checklist with copy checkbox on :' + newSetWithCheckboxOn.size() +'---'+ newSetWithCheckboxOn);

            for(Asset_Checklist__c curRec: newSetWithCheckboxOn){
                setDepartmentId.add(curRec.Department__c);   // department which should not be copied
                setAssetChecklistName.add(curRec.Name);     // asset checklst Name for which to copy
                mapAssetChecklist.put(curRec.Name,curRec);
            }
            System.debug(setDepartmentId);
            System.debug(setAssetChecklistName);
            System.debug(mapAssetChecklist);
           
        for(leaseworks__Aircraft_Proposal__c curAP: listAP){
            setAPId.add(curAP.id);
        }
        System.debug('setAPId:' + setAPId.size());   
        System.debug('listDepartments:' + listDepartments.size()+'::'+ listDepartments);           

        Map<Id,Department__c> mapDepartment = new Map<Id,Department__c>(listDepartments);
        System.debug('mapDepartment:' + mapDepartment.size()+'::'+ mapDepartment);           
            for(Department__c curRec: mapDepartment.values()){
                setDepartmentName.add(curRec.Name);
            }
            
        for(Department__c curDept:listDepartments){    
            if(setDepartmentName.contains(curDept.Name) && !(setDepartmentId.contains(curDept.Id))){
                mapDepartmentToUpdate.put(curDept.Id,curDept);    // all deprtment for which to copy 
            }               
        }

        System.debug('Map of Dept To Update :' + mapDepartmentToUpdate.size() +'::'+ mapDepartmentToUpdate); 
            for(Department__c curDept: mapDepartmentToUpdate.values())
            {
                List<Asset_Checklist__c> listAssetChcklist = curDept.Asset_Checklists__r;
                for(Asset_Checklist__c curRec: listAssetChcklist){
                    if(setAssetChecklistName.contains(curRec.Name)){
                        if((curRec.Is_Completed__c != mapAssetChecklist.get(curRec.Name).Is_Completed__c) && mapAssetChecklist.get(curRec.Name).Copy_To_All_Assets__c){
                            System.debug('Copying Complete for :' + curRec.name +'--'+ curRec.Id);
                            curRec.Is_Completed__c = mapAssetChecklist.get(curRec.Name).Is_Completed__c ;
                            setToUpdate.add(curRec.Id);
                        }
                        if((curRec.Comments__c != mapAssetChecklist.get(curRec.Name).Comments__c)&& mapAssetChecklist.get(curRec.Name).Copy_Comment__c){
                            System.debug('Copying Comment for :' + curRec.name +'--'+ curRec.Id );
                            curRec.Comments__c = mapAssetChecklist.get(curRec.Name).Comments__c ;
                            if(!setToUpdate.contains(curRec.Id))setToUpdate.add(curRec.Id);
                        }
                        if((curRec.Needs_Attention__c != mapAssetChecklist.get(curRec.Name).Needs_Attention__c) && mapAssetChecklist.get(curRec.Name).Copy_Attention__c){
                            System.debug('Copying Attention for :' + curRec.name +'--'+ curRec.Id );
                            curRec.Needs_Attention__c = mapAssetChecklist.get(curRec.Name).Needs_Attention__c ;
                            if(!setToUpdate.contains(curRec.Id)) setToUpdate.add(curRec.Id);
                        }
                    }
                    if(setToUpdate.contains(curRec.Id)) listToUpdate.add(curRec);
                }
            }
            if(listToUpdate.size()>0){
                System.debug('listToUpdate :' + listToUpdate.size() +'::'+ listToUpdate);
                try{
                    update listToUpdate;
                }catch(Exception e){
                    System.debug(e);
                    LeaseWareUtilsLocal.createExceptionLog(e,e.getMessage(),'Asset_Checklist__c', null,'Updating Asset checklist on all Asset In Deals', false);
                }
            } 
            LeaseWareUtilsLocal.insertExceptionLogs();
            System.debug('AssetChecklistTriggerHandlerLocal.updateCheckboxOnAllAssets(-)');
        }

         //Method To get the deal Id's for the current Asset Checklist with Is_Completed field updated
        private static Set<Id> getDealIdForCompleted(){
            System.debug('AssetChecklistTriggerHandlerLocal.getDealIdForCompleted(+)');
            list<Asset_Checklist__c> newList = (list<Asset_Checklist__c>)(trigger.isDelete?trigger.old:trigger.New);

            for(Asset_Checklist__c curRec : newList){
                if(trigger.isUpdate && ((Asset_Checklist__c)trigger.oldMap.get(curRec.Id)).Is_Completed__c != curRec.Is_Completed__c){
                    leaseworks__Aircraft_Proposal__c aircraftPrpsal = mapDeptIdToAP.get(curRec.Department__c);
                    setDealIdComplt.add(aircraftPrpsal.leaseworks__Marketing_Activity__c);
                }else if(trigger.isInsert || trigger.isDelete){
                    leaseworks__Aircraft_Proposal__c aircraftPrpsal = mapDeptIdToAP.get(curRec.Department__c);
                    setDealIdComplt.add(aircraftPrpsal.leaseworks__Marketing_Activity__c);
                }
            }
            System.debug('AssetChecklistTriggerHandlerLocal.getDealIdForCompleted(-)');
            return setDealIdComplt ;
        }

        private static Set<Id> getDealId(){
            System.debug('AssetChecklistTriggerHandlerLocal.gerDealId(+)');
            
            Set<Id> setDealId1 = new Set<Id>();
            Set<Id> setDepartmentId = new Set<Id>();
            Set<Id> setAssetId = new Set<Id>();
            list<Asset_Checklist__c> newList = (list<Asset_Checklist__c>)(trigger.isDelete?trigger.old:trigger.New);

            for(Asset_Checklist__c curAC : newList){
                setDepartmentId.add(curAC.Department__c);           
            } 
            
            for(Department__c curDept:[Select Id, Name,Asset_in_Deal__c from Department__c where id in:setDepartmentId]){
                setAssetId.add(curDept.Asset_in_Deal__c);
            }

            for(leaseworks__Aircraft_Proposal__c curAP:[Select Id,Name,leaseworks__Marketing_Activity__c from leaseworks__Aircraft_Proposal__c where id in:setAssetId]){
                setDealId1.add(curAP.leaseworks__Marketing_Activity__c);
            }
                System.debug('AssetChecklistTriggerHandlerLocal.gerDealId(-)');
            return setDealId1 ;
        }
      
    }